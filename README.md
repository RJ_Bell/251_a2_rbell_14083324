## --------------- MEMBERS ##

*JORGE ALBERTO SOLIS TRUEBA:* \_14073426\_

*ROSS J BELL:* \_14083324\_



### --------------- HOW TO RUN? ###

1. Using command line find the path where the file mybackup.py is located.
2. Inside of the path where mybackup.py is located type "python main.py init".
   This will create a folder named 'myArchive' in *User* folder located.
3. To add files to the archive type "python main.py store `your/source'".
4. To view all the file paths saved type "python main.py list".
   If you want to view any specific files with a particular pattern type "python main.py list File"
5. ~~To recover a file type "python main.py restore File".
   If the program finds more than two files with the same pattern it will display the first 50 files.
   You will then just need to type in the line number of the file that you want to recover.
   This file recovered will be added to your Desktop\\myArchive~~
6. To recover all files and save them into an specific folder type "python main.py restore myRestore"
   This will restore all files and folders in their respected order, this is currently located at.
   restore <foldername> will restore files to that location, this currently defaults to myArchive/myRestore/<foldername>
   for test

### --------------- OUR PROGRAM: ###

* Creates Folders: _User_/myArchive/Objects myArchive/myRestore
* Creates Files:   _User_/myArchive/index.txt myArchive/log.txt

### --------------- COMMIT: ###


AUTHOR |COMMIT ID
----------- | ------------
JORGE ALBERTO SOLIS TRUEBA | 1b6dcde
JORGE ALBERTO SOLIS TRUEBA | af3d57e
JORGE ALBERTO SOLIS TRUEBA | 0b9642d
ROSS J BELL | d7880e3
ROSS J BELL | cd47286  
ROSS J BELL | 5908968
